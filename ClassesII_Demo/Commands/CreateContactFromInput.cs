﻿using ClassesII_Demo.Models;
using System;


namespace ClassesII_Demo.Commands
{
    public static class CreateContactFromInput 
    {
        public static Contact CreateContact(string[] args)
        {
            if (args.Length != 3)
            {
                throw new ArgumentException("Please provide at least 3 arguments. The first argument should be name, the second - phonenumber");
            }
            var contact = new Contact(args[1], args[2]);
            return contact;
        }

    }
}
