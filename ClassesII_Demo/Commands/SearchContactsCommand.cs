﻿using ClassesII_Demo.ICommands;
using ClassesII_Demo.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassesII_Demo.Commands
{
    public class SearchContactsCommand : ICommand
    {
        public string Execute(string[] args)
        {
            return Phonebook.SearchContacts(args[1]);
        }
    }
}
