﻿using ClassesII_Demo.ICommands;
using ClassesII_Demo.Models;

namespace ClassesII_Demo.Commands
{
    public class ListContactsCommand : ICommand
    {
        public string Execute(string [] args)
        {
            if (args.Length > 1)
            {
                string sortOptions = args[1];
                return Phonebook.ListContacts(sortOptions);
            }
            else
            {
                return Phonebook.ListContacts();
            }
        }
    }
}
