﻿using ClassesII_Demo.ICommands;
using ClassesII_Demo.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassesII_Demo.Commands
{
    class RemoveContactCommand : ICommand
    {
        public string Execute(string[] args)
        {
            Contact contactToRemove = CreateContactFromInput.CreateContact(args);
            return Phonebook.RemoveContact(contactToRemove);
        }
    }
}
