﻿using ClassesII_Demo.ICommands;
using ClassesII_Demo.Models;


namespace ClassesII_Demo.Commands
{
    class AddContactCommand : ICommand
    {
        public string  Execute(string[] args)
        {
            Contact contactToAdd = CreateContactFromInput.CreateContact(args);

            Phonebook.AddContact(contactToAdd);

            return $"Created contact " + contactToAdd.GetContactInfo();


        }
    }
}
