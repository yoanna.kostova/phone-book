﻿using ClassesII_Demo.ICommands;
using ClassesII_Demo.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassesII_Demo.Commands
{
    public class UpdateContactCommand : ICommand
    {
        public string Execute(string[] args)
        {
            Contact contactToUpdate = CreateContactFromInput.CreateContact(args);
            return Phonebook.UpdateContact(contactToUpdate);
        }
    }
}
