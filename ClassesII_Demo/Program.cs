﻿using ClassesII_Demo.Infrastructure;

namespace ClassesII_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Engine.Run();
        }
    }
}
