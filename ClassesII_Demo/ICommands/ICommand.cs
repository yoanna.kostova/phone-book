﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassesII_Demo.ICommands
{
    interface ICommand
    {
        public string Execute(string[] args);

    }
}
