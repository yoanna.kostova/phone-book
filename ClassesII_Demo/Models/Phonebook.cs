﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ClassesII_Demo.Models
{
    // TODO: Capitalize every word when naming classes, methods, fields and properties (PhoneBook)
    public static class Phonebook
    {
        private static readonly List<Contact> contacts = new List<Contact>();
        //TODO: research that!
        public static void SeedContacts()
        {
            if (!contacts.Any())
            {
                string json = File.ReadAllText(@"../../../contacts.json");
                var seededContacts = JsonConvert.DeserializeObject<List<Contact>>(json);

                foreach (var contact in seededContacts)
                {
                    contacts.Add(contact);
                }
            }
        }
        //TODO: to locally save the contacts in .txt file
        public static void SaveContacts()
        {
            if (!contacts.Any())
            {
                //string json = File.ReadAllText(@"../../../contacts.json");
                //var seededContacts = JsonConvert.DeserializeObject<List<Contact>>(json);

                //foreach (var contact in seededContacts)
                //{
                //    contacts.Add(contact);
                //}
            }
        }
        /// <summary>
        /// Adds a contact to the existing list of contacts.
        /// </summary>
        /// <param Contact="contact"></param>
        public static void AddContact(Contact contact)
        {
            if (contacts.Any(k => k.Name == contact.Name))
            {
                throw new System.ArgumentException("This contact is already in our database.");
            }
            else
            {
                contacts.Add(contact);
            }
        }
        /// <summary>
        /// Removes contacts from the list of existing contacts
        /// </summary>
        /// <param Contact = "contact"></param>
        /// <returns>
        /// Returns a message if the operation was successful.
        /// </returns>
        public static string RemoveContact(Contact contact)
        {
            // TODO: This can be optimized by using only contacts.Remove(contact)
            // I did that with the previous project but it would be nicer if a message appears
            contacts.Remove(contact);

            return $"Contact {contact.Name} has been removed!";
        }
        /// <summary>
        /// Updates the information of an existing contact.
        /// </summary>
        /// <param Contact="contact"></param>
        /// <returns>
        /// Returns a message wether the contact has been updated or no.
        /// </returns>
        public static string UpdateContact(Contact contact)
        {
            if (!contacts.Any(k => k.Name == contact.Name))
            {
                // This is blurry because throwing an exception (e.g. throw new System.ArgumentException) works like a return - You cannot have 2 or more returns
                // MY CODE: throw new System.ArgumentException("This contact is already in our database.");
                //Why it`s blurry? Because I already have checked wether the contact is existing in the add method, or because I am not using it anywhere?
                return $"Contact {contact.Name} does not exist!";
            }
            else
            {
                contacts.Where(x => x.Name == contact.Name).SingleOrDefault().PhoneNumber = contact.PhoneNumber;
                //Would this work too?
                //in reference tyes - objects, it doesn`t make a copy and if you want to change it it will change it in
                //the collection too.
                //with value types it makes a copy and you have to insert it in the collection again after

                //var currentContact = contacts.Where(x => x.Name == contact.Name).SingleOrDefault();
                //currentContact.PhoneNumber = contact.PhoneNumber;

                // Explanation
                // SingleOrDefault works like TryParse. TryParse (unlike Parse will NOT throw an exception).
                // The same goes for SingleOrDefault - it will return null if Where doesn't find the contact.
                // TODO: Check if currentContact is null before accessing its .PhoneNumber property.
                // If currentContact is null - the program will crash!

                //THANK YOUU!!

                return "Contact updated!";
            }
        }
        /// <summary>
        /// Search among the existing contacts.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>
        /// Returns the details of the contact if the contact exists and a message if it is not.
        /// </returns>
        public static string SearchContacts(string name)
        {
            var builder = new StringBuilder();
            foreach (var contact in contacts)
            {
                if (contact.Name.StartsWith(name))
                {
                    builder.AppendLine(contact.GetContactInfo());
                }
            }
            if (builder == null)
            {
                return "No contacts found.";
            }
            return builder.ToString().TrimEnd();
        }
        /// <summary>
        /// List all the contacts in the phonebook.
        /// The contacts can be listed in a generic order, by name or time created.
        /// </summary>
        /// <param name="optionalParam"></param>
        /// <returns></returns>
        public static string ListContacts(string optionalParam = "")
        {
            List<Contact> contactsOrdered = new List<Contact>();

            if (optionalParam == "name")
            {
                contactsOrdered = contacts.OrderBy(x => x.Name).ToList();
            }
            else if (optionalParam == "time")
            {
                contactsOrdered = contacts.OrderByDescending(x => x.CreatedOn).ToList();
            }
            else
            {
                contactsOrdered = contacts;
            }

            var builder = new StringBuilder();
            foreach (var contact in contactsOrdered)
            {
                builder.AppendLine(contact.GetContactInfo());
            }
            return builder.ToString();
        }
    }
}

