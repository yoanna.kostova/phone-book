﻿using System;

namespace ClassesII_Demo.Models
{
    public class Contact
    {
        private string name;
        private string phoneNumber;

        public Contact(string name, string phoneNumber)
        {
            this.Name = name;
            this.PhoneNumber = phoneNumber;
            this.CreatedOn = DateTime.Now;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Please provide a name.");
                }
                this.name = value;
            }
        }

        public string PhoneNumber
        {
            get
            {
                return this.phoneNumber;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Please provide a phone number.");
                }
                this.phoneNumber = value;
            }

        }

        public DateTime CreatedOn { get; set; }

        public string GetContactInfo()
        {
            return $"{this.Name}: [{this.PhoneNumber}] - Created on {this.CreatedOn.ToString("hh:mm:ss tt")}";
        }
    }
}
