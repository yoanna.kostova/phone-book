﻿using ClassesII_Demo.Commands;
using ClassesII_Demo.ICommands;
using System.Collections.Generic;

namespace ClassesII_Demo.Infrastructure
{
    public static class CommandProcessor
    {
        private static Dictionary<string, ICommand> commands = new Dictionary<string, ICommand>()
        {
            {"1", new AddContactCommand() },
            {"2", new ListContactsCommand() },
            { "3", new RemoveContactCommand() },
            { "4", new UpdateContactCommand() },
            { "5", new SearchContactsCommand() }
        };

    //check again what i did here,cause i have no idea but it works!

        public static string ProcessCommand(string input)
        {
            var args = input.Split();
            if (commands.ContainsKey(args[0]))
            {
                return commands[args[0]].Execute(args);
            }
            return "Try with a valid command.";
        }
    }
}
