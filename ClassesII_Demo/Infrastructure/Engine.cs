﻿using ClassesII_Demo.Models;
using System;

namespace ClassesII_Demo.Infrastructure
{
    public static class Engine
    {
        /// <summary>
        /// Welcome to the system and orientation in the program.
        /// </summary>
        public static void Run()
        {
            Phonebook.SeedContacts();
            Console.WriteLine("Welcome to our application! Please choose an option from the menu:");

            while (true)
            {
                Console.WriteLine("Write - 1 - to add a contact");
                Console.WriteLine("Write - 2 - to list all the contacts");
                Console.WriteLine("Write - 2 - and add \"name\" or \"time\" to list the contacts by name or date created");
                Console.WriteLine("Write - 3 - to remove a contact");
                Console.WriteLine("Write - 4 - to update a contact");
                Console.WriteLine("Write - 5 - to search a contact");

                // input
                var input = Console.ReadLine();
                // process
                try
                {
                    var result = CommandProcessor.ProcessCommand(input);
                    Console.WriteLine(result);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
